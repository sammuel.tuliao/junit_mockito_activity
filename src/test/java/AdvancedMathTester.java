import org.example.AdvancedMath;
import org.example.BasicMath;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AdvancedMathTester {

    @InjectMocks
    AdvancedMath advancedMath = new AdvancedMath();

    @Mock
    BasicMath basicMath;

    @Test //Test 1
    public void testMultiplyDifferenceBy5(){
        when(basicMath.subtract(80.0,20.0)).thenReturn(60.0);
        Assert.assertEquals(advancedMath.multiplyDifferenceBy5(80.0,20.0),300,0);
    }

    @Test //Test 2
    public void testMultiplySumBy5(){
        when(basicMath.add(75.0,25.0)).thenReturn(100.0);
        when(basicMath.multiply(100.0,5.0)).thenReturn(500.0);

        Assert.assertEquals(advancedMath.multiplySumBy5(75.0,25.0),500, 0);
    }

    @Test//Test 3
    public void testSquareOfSum(){
        when(basicMath.add(20.0,20.0)).thenReturn(40.0);
        when(basicMath.add(20.0,20.0)).thenReturn(40.0);
        Assert.assertEquals(advancedMath.squareOfSum(20.0,20.0),1600,0);

        verify(basicMath,atLeast(2)).add(20.0,20.0);
        verify(basicMath,times(2)).add(20.0,20.0);
    }

    @Test //Test 4
    public void testGetPercentage(){
        when(basicMath.divide(50.0,100.0)).thenReturn(0.5);
        Assert.assertEquals(advancedMath.getPercentage(50.0,100.0),50,0);
        reset(basicMath);
        Assert.assertEquals(advancedMath.getPercentage(50.0,100.0),0,0);
    }

    @Test //test 5
    public void testMultiplyDifferenceBy5BDD(){
        given(basicMath.subtract(80.0,20.0)).willReturn(60.0);
        double result = advancedMath.multiplyDifferenceBy5(80.0,20.0);
        Assert.assertEquals(result,300,0);
    }

    @Test //Test 6
    public void testMultiplySumBy5BDD(){
        given(basicMath.add(75.0,25.0)).willReturn(100.0);
        given(basicMath.multiply(100.0,5.0)).willReturn(500.0);
        double result = advancedMath.multiplySumBy5(75.0,25.0);
        Assert.assertEquals(result,500, 0);
    }

    @Test//Test 7
    public void testSquareOfSumBDD(){
        given(basicMath.add(20.0,20.0)).willReturn(40.0);
        given(basicMath.add(20.0,20.0)).willReturn(40.0);
        double result = advancedMath.squareOfSum(20.0,20.0);
        Assert.assertEquals(result,1600,0);
        then(basicMath).should(atLeast(2)).add(20.0,20.0);
        then(basicMath).should(times(2)).add(20.0,20.0);

    }

    @Test //Test 8
    public void testGetPercentageBDD(){
        given(basicMath.divide(50.0,100.0)).willReturn(0.5);
        double result = advancedMath.getPercentage(50.0,100.0);
        Assert.assertEquals(result,50,0);
        reset(basicMath);
        result = advancedMath.getPercentage(50.0,100.0);
        Assert.assertEquals(result,0,0);
    }


}
